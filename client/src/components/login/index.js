import useLoginStore from "../login/"

export default function Login() {
  const handleLogIn = useLoginStore((state) => state.setValue);
  const handleSubmit = (event) => {
    event.preventDefault();
    document.getElementById('heading').scrollIntoView();
    handleLogIn(event.target.value);
  }


  return(
    <div>
    <h1>Log In</h1>
    <form action="" onSubmit={() => handleSubmit}>
      <label>
        <p>Username</p>
        <input type="text" />
      </label>
      <label>
        <p>Password</p>
        <input type="password" />
      </label>
      <div>
        <button type="submit">Submit</button>
      </div>
    </form>
    </div>
  )
}