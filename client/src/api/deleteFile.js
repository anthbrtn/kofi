import baseApiCall from "./baseApi";

const deleteFile = async (filePath) => 
    baseApiCall({
    method: "post",
    url: "/delete",
    data: {
      filePath: filePath,
    },
  });

export default deleteFile;
