import * as fs from "fs";

const getFile = (filePath) => {
  const file = fs.readFileSync(filePath);
  const fileContent = file.toString();
  return fileContent.toString();
};

const postFile = (filePath, updatedContent) => {
  fs.writeFileSync(filePath, updatedContent);
};

const newFile = (fileName) => {
  fs.writeFileSync(process.env.BASE_DIRECTORY + fileName, "");
};

const deleteFile = (fileName) => {
  fs.unlinkSync(process.env.BASE_DIRECTORY + fileName);
};

export { getFile, postFile, newFile, deleteFile };
