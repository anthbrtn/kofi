import create from "zustand";
import { persist } from "zustand/middleware";
// END IMPORT

const useFileTreeStore = create(
  persist(
    (set) => ({
      files: {},
      selectedFile: "",
      setTree: (newTree) => {
        set({
          files: newTree,
        });
      },
      setSelectedFile: (filename) => {
        set({
          selectedFile: filename,
        });
      },
    }),
    {
      name: "filetree",
    }
  )
);

export default useFileTreeStore;
