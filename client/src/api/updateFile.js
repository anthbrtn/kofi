import baseApiCall from "./baseApi";

const updateFile = (filePath, updatedContent) =>
  baseApiCall({
    method: "post",
    url: "/update",
    data: {
      content: updatedContent,
      filePath: filePath,
    },
  });

export default updateFile;
