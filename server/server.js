// stdlib imports
import path from "path";
const __dirname = path.resolve();

// fastify imports
import fastify from "fastify";
import fastifyCors from "fastify-cors";
import fastifyEnv from "fastify-env";
import blipp from "fastify-blipp";
import fastifyJWT from "fastify-jwt";
import fastifyStatic from "fastify-static";

// controller imports
import getTree from "./models/fileTree.js";
import getGlob from "./models/fileGlob.js";
import { getFile, postFile, newFile, deleteFile } from "./models/file.js";

// markdown converter - need this because the client
// uses tiptap, which needs HTML as input
import showdown from "showdown";
import footnotes from "showdown-footnotes";
import turndown from "turndown";

// import environment variables
import dotenv from "dotenv";

dotenv.config({ path: "./.env" });
const baseDir = process.env.BASE_DIRECTORY;
const port = process.env.PORT;

const schema = {
  type: "object",
  required: ["PORT"],
  properties: {
    PORT: { type: "string", default: 5020 },
  },
};

const options = {
  confKey: "config",
  schema: schema,
  // data: process.env,
  dotenv: true,
};

// set up markdown to html file sending
const md2htmlConverter = new showdown.Converter({ extensions: [footnotes] });
md2htmlConverter.setFlavor("github");
md2htmlConverter.setOption("ghCompatibleHeaderId", "true");
md2htmlConverter.setOption("simplifiedAutoLink", "true");
md2htmlConverter.setOption("simpleLineBreaks", "true");
md2htmlConverter.setOption("splitAdjacentBlockquotes", "true");

// set up html to markdown rewriting
// choosing to go with turndown instead of an additional DOM library
// TODO: find a parser that is *actually* bidirectional
const html2mdConverter = new turndown({
  headingStyle: "atx",
  hr: "---",
  bulletListMarker: "-",
  emDelimiter: "*",
});

// set up server and middleware
const server = fastify({ logger: true });
server
  .register(fastifyStatic, {
	  root: path.join(__dirname, './client/build')})
  .register(fastifyEnv, options)
  .register(blipp)
  .register(fastifyCors, {
    origin: "*",
  })
  .register(fastifyJWT, { secret: process.env.API_SECRET })
  .ready((err) => {
    if (err) {
      console.error(err);
      console.log(server.config);
    }
  });

// set up routes
// move these to own file eventually
// file serving

// api calls

server.get("/api/tree", (request, reply) => {
  reply.send(getTree(baseDir));
  console.log(reply)
});

server.get("/api/file", async (request, reply) => {
  const file = md2htmlConverter.makeHtml(getFile(await request.query.f));
  reply.send({ data: file });
});

server.get("/api/glob", async (request, reply) => {
  reply.send(getGlob(baseDir));
});

server.post("/api/update", async (request, reply) => {
  const fileContent = html2mdConverter.turndown(await request.body.content);
  postFile(request.body.filePath, fileContent);
  reply.send("posted " + request.body.filePath);
});

server.post("/api/new", async (request, reply) => {
  let fileName = request.body.fileName;
  newFile(await fileName);
  await reply.send({ filePath: process.env.BASE_DIRECTORY + fileName });
});

server.post("/api/delete", async (request, reply) => {
  const filePath = request.body.filePath;
  deleteFile(filePath);
  await reply.send("deleted " + filePath);
});

const start = async () => {
  try {
    await server.listen(port);
    server.blipp();
  } catch (err) {
    server.log.error(err);
    process.exit(1);
  }
};

start();
