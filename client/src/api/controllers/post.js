import useEditorStore from "../../store/editorStateStore";
import useFileTreeStore from "../../store/fileTreeStore";

import updateFile from "../updateFile";
import newFile from "../newFile";
import deleteFile from "../deleteFile";
import getTree from "../getTree";

const serverUpdateFile = async () => {
  const fileContent = useEditorStore.getState().value;
  const filePath = useEditorStore.getState().filePath;
  await updateFile(filePath, fileContent);
};

const serverNewFile = async (fileName) => {
  const pathFromServer = await newFile(fileName);
  useEditorStore.setState({
    fileName: fileName,
    value: "",
    filePath: pathFromServer,
    fromEditor: false,
  });
};

const serverDeleteFile = async (filePath) => {
  await deleteFile(filePath);
  const newTree = await getTree();
  useFileTreeStore.setState({ files: newTree });
}

export { serverUpdateFile, serverNewFile, serverDeleteFile };
