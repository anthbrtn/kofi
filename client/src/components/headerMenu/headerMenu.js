import useEditorStore from "../../store/editorStateStore";

const HeaderMenu = () => {
  
  const openFile = useEditorStore((state) => state.fileName);
  const characterCount = useEditorStore((state) => state.characterCount);
  const wordCount = useEditorStore((state) => state.wordCount);



  return (
    <div style={{ width: "100%" }}>
      <div style={{ float: "right", paddingTop: "4px", fontSize: "10" }}>
        <span>words: {wordCount}</span>
        &nbsp; | &nbsp;
        <span>char: {characterCount}</span>
        &nbsp; | &nbsp;
        <span>file: {openFile}</span>
      </div>
    </div>
  );
};

export default HeaderMenu;
