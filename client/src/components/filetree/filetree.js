import { Treebeard } from "react-treebeard";

import { serverGetFile } from "../../api/controllers/get";

import useFileTreeStore from "../../store/fileTreeStore";

import treebeardStyle from "./treebeardStyle";

const FileTree = () => {
  const files = useFileTreeStore((state) => state.files);
  const setData = useFileTreeStore((state) => state.setTree);
  const cursor = useFileTreeStore((state) => state.selectedFile);
  const setCursor = useFileTreeStore((state) => state.setSelectedFile);

  const onToggle = (node, toggled) => {
    if (cursor) {
      cursor.active = false;
    }
    node.active = true;
    if (node.children) {
      node.toggled = toggled;
    } else {
      serverGetFile(node.path);
      setCursor(node);
    }
    setData(Object.assign({}, files));
  };

  return (
    <>
  <Treebeard data={files} onToggle={onToggle} 
      style={treebeardStyle} 
      />
      </>
  )
};
export default FileTree;