import api from "./baseApi"

const getTree = () => api({
    method: 'get',
    url: '/tree',
  })
  .then(function (response) {
    console.log(response.data);
      return response.data
  });

export default getTree