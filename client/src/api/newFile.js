import api from "./baseApi";

const newFile = async (fileName) => {
  const response = await api.request({
    method: "post",
    url: "/new",
    data: {
      fileName: fileName,
    },
  })
  return response.data.filePath
};

export default newFile;
