import "../../theme/globalstyles.css"

const treebeardStyle = {
  tree: {
    base: {
      listStyle: "none",
      margin: 0,
      padding: 0,
      backgroundColor: "#fafafa",
      fontFamily: "Sans-Serif",
      fontWeight: 500,
      fontSize: "12px",
      width: "100%",
      height: "100%",
      maxHeight: "600px",
      overflowY: "auto",
      scrollbar: 'hidden'
    },
    node: {
      base: {
        position: "relative",
      },
      link: {
        cursor: "pointer",
        position: "relative",
        padding: "0px 5px",
        display: "block",
      },
      activeLink: {
        background: "#9acefc",
      },
      toggle: {
        base: {
          position: "relative",
          display: "inline-block",
          verticalAlign: "top",
          marginLeft: "-5px",
          height: "24px",
          width: "24px",
        },
        wrapper: {
          position: "absolute",
          top: "50%",
          left: "50%",
          margin: "-12px 0 0 -4px",
          height: "14px",
        },
        height: 7,
        width: 7,
        arrow: {
          fill: "#7a7a7a",
          strokeWidth: 0,
        },
      },
      header: {
        base: {
          display: "inline-block",
          verticalAlign: "top",
          color: "#333",
        },
        connector: {
          width: "2px",
          height: "12px",
          borderLeft: "solid 2px black",
          borderBottom: "solid 2px black",
          position: "absolute",
          top: "0px",
          left: "-21px",
        },
        title: {
          lineHeight: "24px",
          verticalAlign: "middle",
        },
      },
      subtree: {
        listStyle: "none",
        paddingLeft: "19px",
      },
    },
  },
};

export default treebeardStyle;
