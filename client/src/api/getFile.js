import api from "./baseApi";

const getFile = async (filepath) => {
  const response = await api.request({
    method: "get",
    url: "/file",
    params: {
      f: filepath,
    },
  });
  return response.data;
};

export default getFile;
