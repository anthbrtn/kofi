import api from "./baseApi"

const getGlob = () => api({
    method: 'get',
    url: '/glob',
  })
  .then(function (response) {
      return response.data
  });

export default getGlob