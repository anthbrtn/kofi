import { LeftResizable, Fill } from "react-spaces";

import Editor from "../editor/editor";

import SideBar from "../sideBar/sidebar";

import useUIStore from "../../store/UIStore";

const Body = () => {
  const visibility = useUIStore((state) => state.visibility);
  const setVisibility = useUIStore((state) => state.setVisibility);
  const setArrowOrientation = useUIStore((state) => state.setArrowOrientation);

  const calculatedRSize = (visibility, newSize) => {
    setVisibility(newSize);
    if (visibility) {
      let rsize = window.innerWidth - visibility;
      return rsize;
    } else {
      let rsize = window.innerWidth;
      return rsize;
    }
  };

  const resizeEndHandler = (newSize) => {
    setVisibility(newSize);
    if (visibility === 35) {
      setArrowOrientation("right");
    }
  }

  return (
    <div style={{ position: "static" }}>
      <LeftResizable
        className="sidebar"
        size={visibility}
        onResizeEnd={resizeEndHandler}
        minimumSize={35}
        maximumSize={window.innerWidth}
      >
        <SideBar />
      </LeftResizable>
      <Fill
        className="main"
        minimumSize={300}
        size={calculatedRSize}
        scrollable={true}
        maximumSize={window.innerWidth}
      >
        <Editor />
      </Fill>
    </div>
  );
};
export default Body;
