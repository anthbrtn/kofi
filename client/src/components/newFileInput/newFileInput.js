import { serverNewFile } from "../../api/controllers/post";
import { serverGetTree } from "../../api/controllers/get";

import useUIStore from "../../store/UIStore";

import { IoIosCreate } from "react-icons/io";
import Tooltip from "rc-tooltip";
import toast, { Toaster } from "react-hot-toast";

const NewFileInput = () => {
  const inputValue = useUIStore((state) => state.inputValue);
  const setInputValue = useUIStore((state) => state.setInputValue);

  const handleChange = (e) => {
    setInputValue(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log("submitting " + inputValue);
    let fileName = inputValue + ".md";
    serverNewFile(fileName);
    serverGetTree();
    toast(fileName+' created');
  };

  const tooltip = <span className="tooltip">Create file</span>;

  return (
    <form
      onSubmit={handleSubmit}
      className="flexdiv-column"
      style={{
        width: "100%",
        maxWidth: "300px",
      }}
    >
      <Toaster/>
      <h3>New file</h3>
      <div className="flexdiv">
        <input type="text" onChange={handleChange} style={{ width: "100%" }} />
        <Tooltip overlay={tooltip} animation="zoom" trigger="hover">
          <button
            type="submit"
            data-tip="Create file"
            className="new-file-button"
          >
            <IoIosCreate />
          </button>
        </Tooltip>
      </div>
    </form>
  );
};
export default NewFileInput;
