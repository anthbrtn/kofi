# Kofi

STATUS: Alpha. However, the basic functionality works: this app can be deployed to edit markdown files located on the local file server, from changing and autosaving to creating new files.

A live-previewing, local Markdown editor web aplication. Uses `Tiptap` for the editor component, `React` for the UI, `Fastify` for the backend file serving, and `styled-components` for styling.


### Sidebar with file selection, new file creation, and UI controls

<img width="1000" alt="image" src="https://user-images.githubusercontent.com/27690941/126801299-0594939d-d263-4896-9405-a39e135063e6.png">

### Sidebar-free, distraction-free (the header can also be hidden from a toggle in the sidebar)

<img width="1000" alt="image" src="https://user-images.githubusercontent.com/27690941/126799997-82b97885-fd28-4b51-a551-58a9e0d1c68c.png">

## What is this?

This is a web app for editing markdown that's hosted on the same file server as the application itself. You can use a raspberry pi as a local file server, for example, and you can edit markdown files hosted on it. It's designed to be agnostic to file editing -- nothing you edit is locked in to the program, and remains as a plaintext file accessible by any other means on the server's filesystem.

I designed this because while there are plenty of self-hostable note- and text-taking applications out there, there's not a single I could find that just reflects the local file structure. Think of it as the Obsidian approach: you can use this editor, or any other editor under the sun, to edit your files, because the file system itself is the source of truth.

It currently supports reading the directory defined by the API (a simple server that calls `node.js`'s `fs` module), reading files from that directory, and editing those files with changes propagated to the filesystem. All changes are saved after a short (10-stroke) delay. 

New file creation is supported; file deletion is not yet supported (but you can do that from the terminal or file explorer; again this is just one of many possible interfaces to the file).

## How to use

1. Clone the repository into a folder that you'll want to keep it with 

    - `git clone https://github.com/anthbrtn/kofi/` 
  
2. Change to the directory

    - `cd kofi`
  
4. Run the following to prepare the packages for deployment

    - `yarn install` or `npm install`
  
3. Build the client code for serving

    - `cd client && yarn run build`
  
4. Copy `.env.sample` to `.env` (the command `cd ../ && cp .env.sample .env` works), and change the variables so that it: 

    i. knows where your files are (the variable `BASE_DIRECTORY`);
    
    
    ii. knows what port you want to serve the application on (the variable `PORT`). The default is 5020.
    
5. Start the server from the main folder

    - `node server/server.js`
  
6. Open your browser at `localhost:5020` (or the `PORT` you defined in the `.env` file) and work away.

# Caveats
There's no authentication built in yet, so don't expose this to the internet, and make sure that your local network is one you trust.

# TODOs, ordered per category by labour level
## Frontend
- [x] remove extraneous reducers
- [x] Move wordcount, charactercount to store for header rendering
- [x] set up buttons to work
- [x] add the filetree overlay drawer back in
- [x] finalize visual style and demarcate editor area
- [ ] set up `Bibtex` plugin for `pandoc` autocompletion using `TipTap`'s mentions extension
- [ ] build in tabs

## Together with API
- [x] Integrate with backend in single package
- [x] Build out newfile editing
- [ ] build in deletion, renaming through filetree component
- [ ] (MAYBE) Build backend in either `rust` or `golang`

## Packaging
- [x] Ready for static deployment
- [ ] Integrate tests / testing framework
- [ ] Write a `dockerfile` from alpine image
- [ ] Write a basic example `.docker-compose`
