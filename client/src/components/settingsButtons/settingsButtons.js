import { serverGetTree } from "../../api/controllers/get";
import useUIStore from "../../store/UIStore";

import Tooltip from "rc-tooltip";
import { IoMdSync, IoIosArrowUp } from "react-icons/io";


function SettingsButtons() {
  const setHeaderViz = useUIStore((state) => state.setHeaderViz);
  const refresh = <span className="tooltip">Refresh files</span>;
  const hideHeader = <span className="tooltip">Hide header</span>;
  return (
    <div>

      <Tooltip overlay={refresh} placement="top" animation="zoom" trigger="hover">
      <button
        className="settings-button"
        onClick={serverGetTree}
        data-tip="Refresh tree"
      >
        <IoMdSync />
      </button>
      </Tooltip>
      <Tooltip overlay={hideHeader} placement="top" animation="zoom" trigger="hover">
      <button
        className="settings-button"
        onClick={setHeaderViz}
        data-tip="Toggle header"
      >
        <IoIosArrowUp />
      </button>
      </Tooltip>
    </div>
  );
}

export default SettingsButtons;
