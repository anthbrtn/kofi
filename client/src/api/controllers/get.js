import useEditorStore from "../../store/editorStateStore";
import useFileTreeStore from "../../store/fileTreeStore";
import getFile from "../getFile";
import getTree from "../getTree";
import getGlob from "../getGlob";

const serverGetFile = async (filePath) => {
  const newContent = await getFile(filePath);
  let fileName = filePath.split("/").pop();
  useEditorStore.setState({
    filePath: filePath,
    fileName: fileName,
    value: newContent.data,
    fromEditor: false,
  });
};

const serverGetTree = async () => {
  const newTree = await getTree();
  useFileTreeStore.setState({ files: newTree });
};

const serverGetGlob = async () => {
  const arrayOfFiles = await getGlob();
  return arrayOfFiles;

}


export { serverGetFile, serverGetTree, serverGetGlob };
