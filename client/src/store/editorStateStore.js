import create from "zustand";
import { persist } from "zustand/middleware";
// END IMPORT

const useEditorStore = create(
  persist(
    (set) => ({
      value: {},
      filePath: "",
      fileName: "",
      fromEditor: true,
      characterCount: 0,
      wordCount: 0,
      setValue: (newValue) => set((state) => ({ value: newValue })),
      setIsFromEditor: () => set((state) => ({ fromEditor: true })),
      setIsFromApi: () => set({ fromEditor: false }),
      setCharacterCount: (newValue) =>
        set((state) => ({ characterCount: newValue })),
      setWordCount: (newValue) => set((state) => ({ wordCount: newValue })),
    }),
    {
      name: "editor content",
    }
  )
);

export default useEditorStore;
