import useUIStore from "../../store/UIStore";
import { IoIosArrowDropright, IoIosArrowDropleft } from "react-icons/io";
import { IconContext } from "react-icons";
import Tooltip from "rc-tooltip";
import "rc-tooltip/assets/bootstrap_white.css";

const ArrowButton = () => {
  const visibility = useUIStore((state) => state.visibility);
  const setVisibility = useUIStore((state) => state.setVisibility);
  const arrowOrientation = useUIStore((state) => state.arrowOrientation);
  const setArrowOrientation = useUIStore((state) => state.setArrowOrientation);

  function toggleSidebar(event) {
    // I've confused myself a bit with the arrow orientation thing here.
    // The settings of 0 is hacky, but it works.
    if (visibility === 35) {
      setVisibility(200);
      setArrowOrientation(0);
    } else {
      setVisibility(35);
      setArrowOrientation("left");
    }
  }

  const expand = <span className="tooltip">Expand</span>;
  const collapse = <span className="tooltip">Collapse</span>;

  return (
    <>
      <IconContext.Provider value={{ size: 24 }}>
        {/* eslint-disable-next-line eqeqeq */}
        {arrowOrientation === "right" ? (
          <Tooltip overlay={expand} animation="zoom" trigger="hover">
            <button
              className="arrow-button"
              onClick={toggleSidebar}
              data-tip="Expand"
            >
              <IoIosArrowDropright />
            </button>
          </Tooltip>
        ) : (
          <Tooltip overlay={collapse} animation="zoom" trigger="hover">
            <button
              className="arrow-button"
              onClick={toggleSidebar}
              data-tip="Collapse"
            >
              <IoIosArrowDropleft />
            </button>
          </Tooltip>
        )}
      </IconContext.Provider>
    </>
  );
};

export default ArrowButton;
