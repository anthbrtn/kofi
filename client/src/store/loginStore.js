import create from "zustand";
import { persist } from "zustand/middleware";
// END IMPORT

const useLoginStore = create(
  persist(
    (set) => ({
      value: '',
      selectedFile: "",
      setValue: (input) => {
        set({
          value: input,
        });
      }}),
    {
      name: "authentication",
    }
  )
);

export default useLoginStore;
