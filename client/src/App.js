import Body from "./components/containers/body";
import Header from "./components/containers/header.js";

import useThemeStore from "./store/themeStore";
import useUIStore from "./store/UIStore";

import "./theme/globalstyles.css";

import { Fill, Top, ViewPort } from "react-spaces";

import { IconContext } from "react-icons";


// import "@fontsource/dosis";

const App = () => {
  const themeState = useThemeStore((state) => state.theme);
  const headerViz = useUIStore((state) => state.headerViz);

  // if(!token) {
  //   return <>      <GlobalStyle />
  //   <Login setToken={setToken} />
  //   </>
  // } else {

  return (
    <IconContext.Provider value={{ size: 24 }}>

    <ViewPort>
      {headerViz && (
        <Top className="header" size="25px">
          <Header />
        </Top>
      )}
      <Fill>
        <Body />
      </Fill>
    </ViewPort>
    </IconContext.Provider>
  );
};

export default App;
