import create from "zustand";
import { persist } from "zustand/middleware";

const useUIStore = create(
  persist(
    (set) => ({
      visibility: 200,
      inputValue: "",
      arrowOrientation: "left",
      headerViz: true,
      setInputValue: (newValue) => set((state) => ({ inputValue: newValue })),
      setVisibility: (newValue) => set((state) => ({ visibility: newValue })),
      setHeaderViz: () =>
        set((state) => ({
          headerViz: state.headerViz === true ? false : true,
        })),
      setArrowOrientation: () =>
        set((state) => ({
          arrowOrientation:
            state.arrowOrientation === "right" ? "left" : "right",
        })),
    }),
    {
      name: "visibility",
    }
  )
);

export default useUIStore;
