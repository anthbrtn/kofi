import axios from "axios";

const api = axios.create({
  baseURL: `/api`,
  responseType: "json",
});

export default api;
