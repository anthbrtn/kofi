import directoryTree from "directory-tree";

const getTree = (baseDir) => directoryTree(baseDir, { extensions: /\.md/ });

export default getTree;