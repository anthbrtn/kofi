import HeaderMenu from "../headerMenu/headerMenu";

const Header = () => {
  return (
    <div>
      <HeaderMenu />
    </div>
  );
};

export default Header;
