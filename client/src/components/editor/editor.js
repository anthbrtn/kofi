import React, { useEffect } from "react";
import { unstable_batchedUpdates } from "react-dom";

// store
import useEditorStore from "../../store/editorStateStore";

// send off file
import { serverUpdateFile } from "../../api/controllers/post";

// tiptap
import { useEditor, EditorContent, ReactRenderer } from "@tiptap/react";
import StarterKit from "@tiptap/starter-kit";
import Highlight from "@tiptap/extension-highlight";
import Typography from "@tiptap/extension-typography";
import Document from "@tiptap/extension-document";
import Paragraph from "@tiptap/extension-paragraph";
import Text from "@tiptap/extension-text";
import Mention from "@tiptap/extension-mention";
import Blockquote from "@tiptap/extension-blockquote";
import CharacterCount from "@tiptap/extension-character-count";
import { MentionList } from "./MentionList";

import tippy from "tippy.js";

// hacky, but Zustand tip: declaring store update action from outside component
// lets us use batchedUpdates() for async updates, resulting in dramatic
// speed increase
const updateStore = (value) => {
  unstable_batchedUpdates(() => {
    useEditorStore.getState().setValue(value);
  });
};

const Editor = () => {
  // initializing state
  const editorStore = useEditorStore.getState().value;
  const fileName = useEditorStore.getState().fileName;
  const isChangeFromEditor = useEditorStore((state) => state.fromEditor);
  const setChangeFromEditor = useEditorStore((state) => state.setIsFromEditor);
  const setCharacterCount = useEditorStore((state) => state.setCharacterCount);
  const setWordCount = useEditorStore((state) => state.setWordCount);

  // this  makes the onupdate available inside the editor hook
  let onUpdate;
  // this is a hacky way to not get the editor to serialize HTML every time
  let counter = 0;

  // initializing editor
  const editor = useEditor({
    extensions: [
      StarterKit,
      Highlight,
      Typography,
      Document,
      Paragraph,
      Text,
      CharacterCount,
      Mention.configure({
        HTMLAttributes: {
          class: "p",
        },
        suggestion: {
          items: (query) => {
            return [
              "Lea Thompson",
              "Cyndi Lauper",
              "Tom Cruise",
              "Madonna",
              "Jerry Hall",
              "Joan Collins",
              "Winona Ryder",
              "Christina Applegate",
              "Alyssa Milano",
              "Molly Ringwald",
              "Ally Sheedy",
              "Debbie Harry",
              "Olivia Newton-John",
              "Elton John",
              "Michael J. Fox",
              "Axl Rose",
              "Emilio Estevez",
              "Ralph Macchio",
              "Rob Lowe",
              "Jennifer Grey",
              "Mickey Rourke",
              "John Cusack",
              "Matthew Broderick",
              "Justine Bateman",
              "Lisa Bonet",
            ]
              .filter((item) =>
                item.toLowerCase().startsWith(query.toLowerCase())
              )
              .slice(0, 5);
          },
          render: () => {
            let reactRenderer;
            let popup;

            return {
              onStart: (props) => {
                reactRenderer = new ReactRenderer(MentionList, {
                  props,
                  editor: props.editor,
                });

                popup = tippy("body", {
                  getReferenceClientRect: props.clientRect,
                  appendTo: () => document.body,
                  content: reactRenderer.element,
                  showOnCreate: true,
                  interactive: true,
                  trigger: "manual",
                  placement: "bottom-start",
                });
              },
              onUpdate(props) {
                reactRenderer.updateProps(props);

                popup[0].setProps({
                  getReferenceClientRect: props.clientRect,
                });
              },
              onKeyDown(props) {
                return reactRenderer.ref?.onKeyDown(props);
              },
              onExit() {
                popup[0].destroy();
                reactRenderer.destroy();
              },
            };
          },
        },
      }),
      //   History,
    ],
    content: editorStore,
    editorProps: {
      attributes: {
        spellcheck: "true",
        // style: "font-family:Barlow;",
      },
    },
    onUpdate({ editor }) {
      onUpdate && onUpdate(editor);
      setCharacterCount(editor.getCharacterCount());
      setWordCount(editor.state.doc.textContent.split(' ').length);
      // hacky way to only write every 10th change as autosave
      // helps us with large files slowing down localstorage persistence
      counter += 1;
      if (counter % 3 === 0) {
        updateStore(
          editor.getHTML([
            Document,
            Paragraph,
            Text,
            StarterKit,
            Typography,
            Highlight,
            Mention,
            Blockquote,
          ]),
          fileName
        );
        serverUpdateFile();
        counter = 0;
      }
    },
  });

  // this updates state from the backend with new file content
  //need to wait for editor to not be null because state changes are async
  useEffect(() => {
    if (editor != null) {
      if (!isChangeFromEditor) {
        editor.commands.setContent(editorStore);
        setChangeFromEditor();
      }
    }
  });

  return (
    <div className="editor-container">
      <EditorContent editor={editor} value={editorStore} />
    </div>
  );
};

export default Editor;
