import FileTree from "../filetree/filetree";

import NewFileInput from "../newFileInput/newFileInput";
import SettingsButtons from "../settingsButtons/settingsButtons";
import ArrowButton from "../arrowButton/arrowButton";

import { Left, Fill } from "react-spaces";

function SideBar() {
  return (
    <>
      <Left className="arrow-container" size={35}>
        <ArrowButton />
      </Left>
      <Fill className="sidebar-tools-container">
        <div
          className="flexdiv-column"
          style={{
            display: "flex",
            flexDirection: "column",
          }}
        >
          <NewFileInput />
          <h2>Files</h2>
          <div className="flexdiv">
            <FileTree />
          </div>
          <div
            className="flexdiv"
            style={{ position: "absolute", bottom: "20px" }}
          >
            <SettingsButtons />
          </div>
        </div>
      </Fill>
    </>
  );
}

export default SideBar;
